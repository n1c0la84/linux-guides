#installation
yay -S tuned-git

#enable and start the service
sudo systemctl enable --now tuned

#check active profile
tuned-adm active

#list all avalaible profiles
tuned-adm list

#activate profile
tuned-adm profile virtual-guest

#turn it off
tuned-adm off